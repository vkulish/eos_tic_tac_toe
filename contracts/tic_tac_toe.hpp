#include <eosiolib/eosio.hpp>

class tic_tac_toe : public eosio::contract {
   public:
      tic_tac_toe( account_name self ):contract(self){}

      struct game {
         static const uint16_t board_width = 3;
         static const uint16_t board_height = board_width;
         game() { 
            initialize_board(); 
         }
         account_name          challenger;
         account_name          host;
         account_name          turn; 
         account_name          winner = N(none); 
         std::vector<uint8_t>  board;

         void initialize_board() {
            board = std::vector<uint8_t>(board_width * board_height, 0);
         }

         void reset_game() {
            initialize_board();
            turn = host;
            winner = N(none);
         }

         auto primary_key() const { return challenger; }
         EOSLIB_SERIALIZE( game, (challenger)(host)(turn)(winner)(board))
      };

      typedef eosio::multi_index< N(games), game> games;

      void create(const account_name& challenger, const account_name& host);

      void restart(const account_name& challenger, const account_name& host, const account_name& by);

      void close(const account_name& challenger, const account_name& host);

      void move(const account_name& challenger, const account_name& host, const account_name& by, const uint16_t& row, const uint16_t& column);
      
};